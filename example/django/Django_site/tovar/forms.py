# -*- coding: utf-8 -*-

from django import forms

from .models import Tovar 


class TagListForm(forms.Form):
        tag_list = forms.CharField(label='New', required=False)


class TovarForm(forms.Form):
    
    title = forms.CharField(label="Name", max_length=256)
    atricle = forms.CharField(label="Artcile", max_length=16)
    quantity = forms.IntegerField(label="Quantity", min_value=0)
    description = forms.CharField(label="Description", widget=forms.Textarea, required=False)
    

class TovarModelForm(forms.ModelForm):
    
    class Meta:
            model = Tovar
            fields = ['title', 'article', 'quantity', 'description']
            
        
