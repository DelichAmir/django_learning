# -*- coding: utf-8 -*-

from django.shortcuts import get_object_or_404, render

from ..models import Tovar
from  ..forms import TagListForm
from restful import restful


@restful
def tags(request, id_tovar):
    tovar = get_object_or_404(Tovar, pk=id_tovar)
    form = TagListForm()
    return render(request, 'tovar/tags.html', locals())


@tags.method('POST')
def tags(request, id_tovar):
    form = TagListForm(request.POST)
    if form.is_valid():
        tag_list = form.cleaned_data['tag_list']
        tovar = get_object_or_404(Tovar, pk=id_tovar)
        for pk_tag in map(str.strip, tag_list.split(',')):
            tag = Tag.get(pk=1)
            tovar.tags.add(tag)
        tovar.save()
    
