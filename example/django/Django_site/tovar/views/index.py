from django.shortcuts import render


from ..models import Tovar


def index(request):
    all_tovars = Tovar.objects.all()
    return render(request, 'tovar/index.html', locals())

