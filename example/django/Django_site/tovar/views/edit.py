import sys

from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse

from restful import restful
from ..models import Tovar
from ..forms import TovarForm, TovarModelForm

@restful
def edit(request, id_tovar):
    
    python_path = sys.path
    tovar = get_object_or_404(Tovar, pk=id_tovar)
    form = TovarModelForm(instance=tovar)
    return render(request, 'tovar/tovar_edit.html', locals()) 

@edit.method('POST')
def edit(request, id_tovar):
    
    raw_data = dict()
    raw_data.update(request.POST)
    
    tovar = get_object_or_404(Tovar, pk=id_tovar)
    
    form = TovarModelForm(request.POST, instance=tovar)
    if form.is_valid():
        form.save()
        return render(request, 'tovar/tovar_show.html', locals()) 
    else:
        return render(request, 'tovar/tovar_edit.html', locals()) 

